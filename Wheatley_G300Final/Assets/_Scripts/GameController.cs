﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    //public GameObject hudCanvas;

    public float countdownTime;
    public PlayerHealth playerHealth;
    public Text timerText;
    public Animator anim;
    public SimplePlatformController spc;
    public Transform[] artifactSpawns;
    public GameObject artifactPrefab;
    public AudioSource aS;
    public AudioClip bgm;
    public AudioClip victoryClip;
    public AudioClip defeatClip;
    public Rigidbody2D playerRb2d;

    float restartTime;
    float loseRestartDelay = 7.058f;
    float winRestartDelay = 4f;
    bool[] hasSpawnedAt;
    int numToSpawn = 5;
    bool gameOver = false;
    
    
    // Use this for initialization
	void Awake ()
    {
        countdownTime = 45f;
	}

    private void Start()
    {
        aS = GetComponent<AudioSource>();

        hasSpawnedAt = new bool[artifactSpawns.Length];
        int randomIndex = Random.Range(0, artifactSpawns.Length);
        for (int i = 0; i < numToSpawn; i++)
        {
            while (hasSpawnedAt[randomIndex] == true)
                randomIndex = Random.Range(0, artifactSpawns.Length);
            Instantiate(artifactPrefab, artifactSpawns[randomIndex].position, Quaternion.identity);
            hasSpawnedAt[randomIndex] = true;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        countdownTime -= Time.deltaTime;

        if (countdownTime <= 0f && spc.isWin == false)
        {
            if (!gameOver)
            {
                gameOver = true;
                aS.Stop();
                aS.PlayOneShot(defeatClip);
                aS.volume = 0.5f;
                aS.loop = false;
            }
            playerRb2d.simulated = false;
            anim.SetTrigger("gameOver");
            restartTime += Time.deltaTime;
            if (restartTime >= loseRestartDelay)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        timerText.text = "Time: " + countdownTime.ToString("#.##");

        if (playerHealth.currentHealth <= 0 && spc.isWin == false)
        {
            if (!gameOver)
            {
                gameOver = true;
                aS.Stop();
                aS.PlayOneShot(defeatClip);
                aS.volume = 0.5f;
                aS.loop = false;
            }
            playerRb2d.simulated = false;
            anim.SetTrigger("gameOver");
            restartTime += Time.deltaTime;
            if (restartTime >= loseRestartDelay)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        if (spc.isWin == true)
        {
            print("WIN");
            anim.SetTrigger("levelComplete");
            restartTime += Time.deltaTime;
            if (restartTime >= winRestartDelay)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
	}
}
