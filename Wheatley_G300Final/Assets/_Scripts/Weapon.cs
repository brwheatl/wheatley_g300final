﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    float weaponTime;
    
    // Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update ()
    {
        weaponTime += Time.deltaTime;
        if (weaponTime >= 0.1f)
        {
            EndSlash();
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            EnemyHealth eh = collision.GetComponent<EnemyHealth>();
            eh.TakeDamage();
        }
    }

    void EndSlash()
    {
        Destroy(gameObject);
    }
}
