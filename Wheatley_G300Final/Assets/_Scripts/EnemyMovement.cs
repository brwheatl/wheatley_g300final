﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {

    public GameObject player;
    public Vector3[] movePoints;

    float moveSpeed = 3f;
    Rigidbody2D rb2d;
    float knockbackForce = 750f;
    int currentPoint = 0;
    SpriteRenderer sprite;
    
    // Use this for initialization
	void Start ()
    { 
        rb2d = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        movePoints = new Vector3[2];
        movePoints[0] = transform.position + Vector3.left * 2.5f;
        movePoints[1] = transform.position + Vector3.right * 2.5f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Vector3.Distance(transform.position, movePoints[currentPoint]) < 1f)
        {
            currentPoint += 1;
        }
        if (currentPoint >= movePoints.Length)
        {
            currentPoint = 0;
        }

        if (currentPoint == 0)
        {
            sprite.flipX = false;
        }else if (currentPoint == 1)
        {
            sprite.flipX = true;
        }

        transform.position = Vector3.MoveTowards(transform.position, movePoints[currentPoint], moveSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Weapon"))
        {
            Vector3 knockDirection = transform.position - collision.transform.position;
            knockDirection = knockDirection.normalized;
            rb2d.AddForce(knockDirection * knockbackForce);
        }

        if (collision.CompareTag("WeaponVert"))
        {
            rb2d.AddForce(transform.up * knockbackForce);
        }
    }
}
