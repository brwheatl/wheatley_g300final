﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimplePlatformController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    public GameObject weaponPrefab;
    public Transform weaponPosition;
    public int artifactCount;
    public Text winText;
    public bool isWin = false;
    public bool isDead = false;
    public GameObject weaponVertPrefab;
    public GameController gc;
    public AudioClip jumpClip;
    public AudioClip attackClip;
    public AudioClip victoryClip;
    public AudioClip defeatClip;
    public AudioClip artifactClip;
    public PlayerHealth ph;
    public Text artifactText;

    bool isGrounded = false;
    Animator anim;
    Rigidbody2D rb2d;
    float slashTime;
    bool isSlashing = false;
    AudioSource aS;
    float sfxVol;
    Vector3 facingVector;

	// Use this for initialization
	void Awake ()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        aS = GetComponent<AudioSource>();
	}

    private void Start()
    {
        artifactCount = 0;
    }

    // Update is called once per frame
    void Update ()
    {
        isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        sfxVol = Random.Range(0.4f, 0.6f);

        artifactText.text = "Artifacts: " + artifactCount;

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            jump = true;
            aS.volume = sfxVol;
            aS.PlayOneShot(jumpClip);
        }

        if (Input.GetButtonDown("Fire1") && isSlashing == false)
        {
            Slash();
            isSlashing = true;
            aS.volume = sfxVol;
            aS.PlayOneShot(attackClip);
        }

        if (Input.GetButtonDown("Fire2") && isSlashing == false)
        {
            SlashVert();
            isSlashing = true;
            aS.volume = sfxVol;
            aS.PlayOneShot(attackClip);
        }

        if (isSlashing == true)
        {
            slashTime += Time.deltaTime;
        }

        if (slashTime >= 0.15f)
        {
            isSlashing = false;
            slashTime = 0f;
        }

        if (facingRight == true)
        {
            weaponPrefab.GetComponent<SpriteRenderer>().flipX = false;
            weaponVertPrefab.GetComponent<SpriteRenderer>().flipX = false;
        }else if (facingRight == false)
        {
            weaponPrefab.GetComponent<SpriteRenderer>().flipX = true;
            weaponVertPrefab.GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
        {
            rb2d.AddForce(Vector2.right * h * moveForce);
        }

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
        {
            rb2d.velocity = new Vector2 (Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
        }

        if (h > 0 && !facingRight)
        {
            Flip();
        } else if (h < 0 && facingRight)
        {
            Flip();
        }

        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }

    void Flip()
    {
        facingRight =! facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void Slash()
    {
        GameObject weaponClone = Instantiate(weaponPrefab, weaponPosition.transform.position, weaponPosition.transform.rotation) as GameObject;
    }

    void SlashVert()
    {
        GameObject weaponVertClone = Instantiate(weaponVertPrefab, weaponPosition.transform.position, weaponPosition.transform.rotation) as GameObject;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Artifact"))
        {
            Collect();
        }

        if (collision.CompareTag("Portal") && artifactCount >= 5)
        {
            isWin = true;
            gc.aS.Stop();
            aS.PlayOneShot(victoryClip);
            rb2d.simulated = false;
        }
    }

    void Collect()
    {
        artifactCount += 1;
        aS.PlayOneShot(artifactClip);
    }
}
