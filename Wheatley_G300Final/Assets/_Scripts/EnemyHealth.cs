﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public int health = 5;
    public AudioClip hurtClip;

    AudioSource aS;
    
    // Use this for initialization
	void Start (){
        aS = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (health <= 0)
        {
            Die();
        }
    }

    public void TakeDamage()
    {
        health -= 1;
        aS.PlayOneShot(hurtClip);
    }
    
    void Die()
    {
        Destroy(gameObject);
    }
}
