﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnScript : MonoBehaviour {

    public Transform enemySpawn;
    public GameObject enemy;

    int spawnLimit = 1;
    float spawnCheck;
    
    // Use this for initialization
	void Start ()
    {
        spawnCheck = Random.Range(0f, 1f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (spawnCheck >= 0.5f && spawnLimit != 0)
        {
            Spawn();
        }
    }

    void Spawn()
    {
        Instantiate(enemy, enemySpawn.position, Quaternion.identity);
        spawnLimit -= 1;
    }
}
