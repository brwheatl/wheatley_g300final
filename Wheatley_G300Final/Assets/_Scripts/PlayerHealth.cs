﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public int currentHealth = 5;
    public AudioClip hurtClip;
    public bool isDead;
    public Text healthText;

    float timeBtwDamage = 0.3f;
    float damageTime = 0f;
    float immunityTime = 1f;
    AudioSource aS;

    // Use this for initialization
    void Start ()
    {
        aS = GetComponent<AudioSource>();
        aS.volume = 0.5f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        immunityTime -= Time.deltaTime;

        damageTime += Time.deltaTime;

        healthText.text = "Health: " + currentHealth;
        if (currentHealth <= 0)
        {
            isDead = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy") && damageTime >= timeBtwDamage && immunityTime <= 0)
        {
            PlayerTakeDamage();
        }
    }

    void PlayerTakeDamage()
    {
        damageTime = 0;
        currentHealth -= 1;
        aS.PlayOneShot(hurtClip);
    }
}
